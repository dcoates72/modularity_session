proj=$(notdir $(CURDIR) )

all:
	pdflatex $(proj).tex
	bibtex $(proj)
	pdflatex $(proj).tex
	pdflatex $(proj).tex

clean:
	rm -f $(proj).vrb $(proj).snm $(proj).out *.aux $(proj).toc $(proj).nav $(proj).log $(proj).bbl $(proj).blg ${proj.aux} *.log

view:
	evince $(proj).pdf 2>/dev/null &

pull:
	git checkout -- $(proj).pdf
	git pull
 
handout:
	gs -q -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dPDFSETTINGS=/ebook -sOutputFile=$(proj)-low.pdf $(proj).pdf

img/*.png: img/*.jpg
	convert $< $<

img/%.png: img/%.jpg
	convert $< $@
	rm $<
